const config = {
    isUAT: true,
    shop_id: null, // parseInt(process.env.SHOPEE_SHOP_ID),
    access_token: null, //'677a7461664e77465153715a5857476e', // if call private API
    partner_id: process.env.SHOPEE_PARTNER_ID,
    partner_key: process.env.SHOPEE_PARTNER_KEY,
    redirect_uri: process.env.SHOPEE_REDIRECT_URI, // callback url when perform OAuth
    webhook_url: process.env.SHOPEE_WEBHOOK_URL,
    verbose: true // show more logs
}


module.exports = config