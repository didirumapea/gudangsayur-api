const shopeeApi = require('./shopee-core');

class ShopeeOrder {
    constructor(config) {
        if (config === null || config === undefined) {
            throw new Error("config required");
        }

        this.config = config;

        // BUILD PALYLOAD
        let payload = {
            isUAT: process.env.NODE_ENV == "dev" ? true : false,
            partner_id: this.config.partner_id,
            partner_key: this.config.partner_key,
            verbose: process.env.NODE_ENV == "dev" ? true : false, // show more logs
        }

        if (this.config.access_token) {
            payload.access_token = this.config.access_token;
        }
        if (this.config.shop_id) {
            payload.shop_id = this.config.shop_id;
        }

        this.shopeeApi = new shopeeApi(payload);
    }

    listOrderStatus() {
        return ["UNPAID", "READY_TO_SHIP", "PROCESSED", "SHIPPED", "COMPLETED", "IN_CANCEL", "CANCELLED", "INVOICE_PENDING"];
    }

    getOrders(time_from, time_to, order_status) {
        // INITIAL GET ORDER
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/order/get_order_list", timestamp); //sign private API

        // PAYLOAD REQUEST GET ORDER
        const payloadQuery = {
            sign,
            time_range_field: 'create_time',
            response_optional_fields: 'order_status',
            time_from,
            time_to,
            order_status,
            page_size: 100,
            shop_id: this.config.shop_id,
            access_token: this.config.access_token,
            cursor: "",
            timestamp,
            partner_id: this.config.partner_id,
        }

        // REQUEST API SHOPEE ORDER
        let orderData = this.shopeeApi.get('/api/v2/order/get_order_list', payloadQuery, {}, (err, res, body) => {
            if (err) {
                return err;
            }
            return body;
        });

        return orderData;
    }

    getDetailOrder(order_sn_list) {
        // INITIAL GET ORDER
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/order/get_order_detail", timestamp); //sign private API

        // PAYLOAD REQUEST GET ORDER
        const payloadQuery = {
            sign,
            shop_id: this.config.shop_id,
            access_token: this.config.access_token,
            order_sn_list,
            response_optional_fields: "recipient_address,item_list,invoice_data,shipping_carrier"
        }

        // REQUEST API SHOPEE ORDER
        let orderDetail = this.shopeeApi.get('/api/v2/order/get_order_detail', payloadQuery, {}, (err, response, body) => {
            if (err) {
                return err;
            }
            return body;
        });

        return orderDetail;
    }

    getShipmentList(req, res) {
        // INITIAL GET ORDER
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/order/get_shipment_list", timestamp); //sign private API

        // PAYLOAD REQUEST GET ORDER
        const payloadQuery = {
            sign,
            shop_id: this.config.shop_id,
            access_token: this.config.access_token,
            page_size: req.query.page_size,
        }

        // REQUEST API SHOPEE ORDER
        this.shopeeApi.get('/api/v2/order/get_shipment_list', payloadQuery, {}, (err, response, body) => {
            if (err) {
                return err;
            }
            // RETURN DATA
            res.status(200).json({
                success: 1,
                data: body
            });
            // console.log(body)
        });
    }

    splitOrder(req, res) {
        // INITIAL GET ORDER
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/order/split_order", timestamp); //sign private API

        // PAYLOAD REQUEST GET ORDER
        const payloadQuery = {
            sign,
            shop_id: this.config.shop_id,
            access_token: this.config.access_token,
            page_size: req.query.page_size,
            order_sn: req.body.order_sn,
            package_list: req.body.package_list
        };

        // REQUEST API SHOPEE ORDER
        this.shopeeApi.post('/api/v2/order/split_order', payloadQuery, req.body, (err, response, body) => {
            if (err) {
                console.log(err)
            }
            // RETURN DATA
            res.status(200).json({
                success: 1,
                data: body
            });
            // console.log(body)
        });
    }

    unsplitOrder(req, res) {
        // INITIAL GET ORDER
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/order/unsplit_order", timestamp); //sign private API

        // PAYLOAD REQUEST
        const payloadQuery = {
            sign,
            // shop_id: this.config.shop_id,
            // access_token: this.config.access_token,
            order_sn: req.body.order_sn,
        };

        // REQUEST API SHOPEE ORDER
        this.shopeeApi.post('/api/v2/order/unsplit_order', payloadQuery, req.body, (err, response, body) => {
            if (err) {
                console.log(err)
            }
            // RETURN DATA
            res.status(200).json({
                success: 1,
                data: body
            });
            // console.log(body)
        });
    }

    async cancelOrder(dataOrder) {
        // INITIAL GET ORDER
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/order/cancel_order", timestamp); //sign private API

        // PAYLOAD REQUEST GET ORDER
        const payloadQuery = {
            sign,
            shop_id: this.config.shop_id,
            access_token: this.config.access_token,
            partner_id: this.config.partner_id,
            timestamp: this.config.timestamp,
        };

        // REQUEST API SHOPEE ORDER
        let cancelOrder = await this.shopeeApi.post('/api/v2/order/cancel_order', payloadQuery, dataOrder, (err, response, body) => {
            if (err) {
                return err;
            }
            return body;
        });

        return cancelOrder;
    }

    setNote(req, res) {
        // INITIAL GET ORDER
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/order/set_note", timestamp); //sign private API

        // PAYLOAD REQUEST GET ORDER
        const payloadQuery = {
            sign,
            shop_id: this.config.shop_id,
            access_token: this.config.access_token,
        };

        // REQUEST API SHOPEE ORDER
        this.shopeeApi.post('/api/v2/order/set_note', payloadQuery, req.body, (err, response, body) => {
            if (err) {
                console.log(err)
            }
            // RETURN DATA
            res.status(200).json({
                success: 1,
                data: body
            });
            // console.log(body)
        });
    }

    handleBuyerCancellation(req, res) {
        // INITIAL GET ORDER
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/order/handle_buyer_cancellation", timestamp); //sign private API

        // PAYLOAD REQUEST GET ORDER
        const payloadQuery = {
            sign,
            shop_id: this.config.shop_id,
            access_token: this.config.access_token,
        };

        // REQUEST API SHOPEE ORDER
        this.shopeeApi.post('/api/v2/order/handle_buyer_cancellation', payloadQuery, req.body, (err, response, body) => {
            if (err) {
                console.log(err)
            }
            // RETURN DATA
            res.status(200).json({
                success: 1,
                data: body
            });
            // console.log(body)
        });
    }

    addInvoiceData(req, res) {
        // INITIAL GET ORDER
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/order/add_invoice_data", timestamp); //sign private API

        // PAYLOAD REQUEST GET ORDER
        const payloadQuery = {
            sign,
            // shop_id: this.config.shop_id,
            // access_token: this.config.access_token,
        };

        // REQUEST API SHOPEE ORDER
        this.shopeeApi.post('/api/v2/order/add_invoice_data', payloadQuery, req.body, (err, response, body) => {
            if (err) {
                console.log(err)
            }
            // RETURN DATA
            res.status(200).json({
                success: 1,
                data: body
            });
            // console.log(body)
        });
    }

    getPendingBuyerInvoiceOrder(req, res) {
        // INITIAL GET ORDER
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/order/get_pending_buyer_invoice_order_list", timestamp); //sign private API

        // PAYLOAD QUERY REQUEST
        const payloadQuery = {
            sign,
            // shop_id: this.config.shop_id,
            // access_token: this.config.access_token,
        };

        // REQUEST API
        this.shopeeApi.get('/api/v2/order/get_pending_buyer_invoice_order_list', payloadQuery, req.body, (err, response, body) => {
            if (err) {
                console.log(err)
            }
            // RETURN DATA
            res.status(200).json({
                success: 1,
                data: body
            });
            // console.log(body)
        });
    }

    //LOGISTICS
    getShippingParameter(req, res) {
        // INITIAL GET ORDER
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/logistics/get_shipping_parameter", timestamp); //sign private API
        // PAYLOAD QUERY REQUEST
        const payloadQuery = {
            sign,
        };

        // REQUEST API
        this.shopeeApi.get('/api/v2/logistics/get_shipping_parameter', payloadQuery, {}, (err, response, body) => {
            if (err) {
                console.log(err)
            }
            // RETURN DATA
            res.status(200).json({
                success: 1,
                data: body
            });
            // console.log(body)
        });
    }

    shipOrder(req, res) {
        // INITIAL LOGISTICS
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/logistics/ship_order", timestamp); //sign private API

        // PAYLOAD QUERY REQUEST
        const payloadQuery = {
            sign,
        };

        // REQUEST API
        this.shopeeApi.post('/api/v2/logistics/ship_order', payloadQuery, req.body, (err, response, body) => {
            if (err) {
                console.log(err)
            }
            // RETURN DATA
            res.status(200).json({
                success: 1,
                data: body
            });
            // console.log(body)
        });
    }

    getTrackingNumber(req, res) {
        // INITIAL GET ORDER
        const timestamp = Math.floor(new Date() / 1000);
        const sign = this.shopeeApi.isValidSignature("/api/v2/logistics/get_tracking_number", timestamp); //sign private API
        // PAYLOAD QUERY REQUEST
        const payloadQuery = {
            sign,
            ...req.query
        };

        // REQUEST API
        this.shopeeApi.get('/api/v2/logistics/get_tracking_number', payloadQuery, {}, (err, response, body) => {
            if (err) {
                console.log(err)
            }
            // RETURN DATA
            res.status(200).json({
                success: 1,
                data: body
            });
            // console.log(body)
        });
    }
}

module.exports = ShopeeOrder;