var crypto = require('crypto');
const request = require("request");

class ShopeeApi {
    constructor(config) {
        if (config === null || config === undefined) {
            throw new Error("config required");
        }
        this.config = config;

        if (this.config.partner_id) {
            if (
                typeof this.config.partner_id === "string" ||
                this.config.partner_id instanceof String
            ) {
                this.config.partner_id = Number(this.config.partner_id);
            }
        }

        if (this.config.verbose !== true) {
            this.config.verbose = false;
        }
    }

    getBaseUrl() {
        return `https://partner${this.config.isUAT ? ".test-stable" : ""
            }.shopeemobile.com`;
    }

    buildAuthURL(token, isCancel = false) {
        const timestamp = Math.floor(new Date() / 1000);;

        var hmac = crypto.createHmac('sha256', this.config.partner_key);
        var data = hmac.update(this.config.partner_id + "/api/v2/shop/auth_partner" + timestamp);
        var gen_hmac = data.digest('hex');

        let authUrl = `${this.getBaseUrl()}/api/v2/shop/`;
        authUrl += isCancel ? "cancel_auth_partner" : "auth_partner";
        authUrl += `?partner_id=${this.config.partner_id}`;
        authUrl += `&sign=${gen_hmac}`;
        authUrl += `&redirect=${this.config.redirect_uri}?token=${token}`
        authUrl += `&timestamp=${timestamp}`;
        return authUrl;
    }

    buildCancelAuthUrl() {
        return this.buildAuthURL(true);
    }

    generateAuthorization(path, timestamp) {
        var hmac = crypto.createHmac('sha256', this.config.partner_key);
        var data = hmac.update(this.config.partner_id + path + timestamp);
        var gen_hmac = data.digest('hex');
        return gen_hmac;
    }

    isValidSignature(path, timestamp) {
        var message = this.config.partner_id + path + timestamp + this.config.access_token + this.config.shop_id;
        var hmac = crypto.createHmac('sha256', this.config.partner_key);
        var data = hmac.update(message);
        var gen_hmac = data.digest('hex');
        return gen_hmac;

    }

    makeRequest(endpoint, query, data, method, callback = null) {

        const cloneQuery = query === null || query === undefined ? {} : query;
        cloneQuery.partner_id = this.config.partner_id;
        cloneQuery.timestamp = Math.floor(new Date() / 1000);

        if (this.config.shop_id) {
            cloneQuery.shop_id = this.config.shop_id;
        }

        if (this.config.access_token) {
            cloneQuery.access_token = this.config.access_token;
        }
        const self = this;

        const options = {
            baseUrl: this.getBaseUrl(),
            uri: endpoint,
            method: method.toUpperCase() || "POST",
            headers: {},
            json: data,
            qs: query
        };

        self.verboseLog(`REQUEST: ${JSON.stringify(options)}\n`);

        const promise = new Promise(function (resolve, reject) {
            request(options, function (err, res, body) {
                if (err) {
                    self.verboseLog(`ERROR: ${err}`);
                    return reject(err);
                }

                self.verboseLog(`STATUS: ${res.statusCode}\n`);
                self.verboseLog(`HEADERS: ${JSON.stringify(res.headers)}\n`);
                self.verboseLog(`BODY: ${JSON.stringify(body)}\n`);
                return resolve({ body, res });
            });
        });

        if (callback === null) {
            return promise;
        }

        return promise
            .then(function ({ body, res }) {
                return callback(null, res, body);
            })
            .catch(function (err) {
                return callback(err, null, null);
            });
    }

    get(endpoint, query, data = null, callback = null) {
        return this.makeRequest(endpoint, query, data, "GET", callback);
    }

    post(endpoint, query, data = null, callback = null) {
        return this.makeRequest(endpoint, query, data, "POST", callback);
    }

    verboseLog(msg) {
        if (this.config.verbose) {
            console.log(msg);
        }
    }
}

module.exports = ShopeeApi;
