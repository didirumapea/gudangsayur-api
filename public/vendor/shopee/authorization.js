const shopeeApi = require('./shopee-core');

class ShopeeAuth {

    constructor(config) {
        if (config === null || config === undefined) {
            throw new Error("config required");
        }

        this.config = config;

        // BUILD PALYLOAD
        let payload = {
            isUAT: process.env.NODE_ENV == "dev" ? true : false,
            partner_id: this.config.partner_id,
            partner_key: this.config.partner_key,
            verbose: process.env.NODE_ENV == "dev" ? true : false, // show more logs
        }

        if (this.config.access_token) {
            payload.access_token = this.config.access_token;
        }
        if (this.config.shop_id) {
            payload.shop_id = this.config.shop_id;
        }

        this.shopeeApi = new shopeeApi(payload);
    }

    /**
     * generateAccessToken POST /api/v2/auth/token/get
     * @param {String} code
     * @param {Number} shop_id
     * @param {Number} partner_id
     * @param {String} sign
     * code  // require
     * shop_id  // require
     * partner_id // require
     * sign // require
     */
    async generateAccessToken(code, shop_id) {
        let timestamp = Math.floor(new Date() / 1000);
        let sign = await this.shopeeApi.generateAuthorization('/api/v2/auth/token/get', timestamp);
        let authData = await this.shopeeApi.post(`/api/v2/auth/token/get`, { sign }, { code, shop_id: parseInt(shop_id), partner_id: parseInt(this.config.partner_id), sign }, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });

        return authData;
    }

    /**
     * refreshAccessToken POST /api/v2/auth/access_token/get
     * @param {String} refresh_token
     * @param {Number} shop_id
     * refresh_token  // require
     * shop_id  // require
     * partner_id // require
     */
    async refreshAccessToken(refresh_token, shop_id) {
        let timestamp = Math.floor(new Date() / 1000);
        let sign = await this.shopeeApi.generateAuthorization('/api/v2/auth/access_token/get', timestamp);
        let authData = await this.shopeeApi.post(`/api/v2/auth/access_token/get`, { sign }, { refresh_token, shop_id: parseInt(shop_id), partner_id: parseInt(this.config.partner_id) }, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });

        return authData;
    }

    /**
     * sellerInfo GET /api/v2/shop/get_shop_info
     *
     */
    async sellerInfo() {
        let timestamp = Math.floor(new Date() / 1000);
        let sign = this.shopeeApi.isValidSignature("/api/v2/shop/get_shop_info", timestamp); //sign private API
        let shopData = await this.shopeeApi.get('/api/v2/shop/get_shop_info', { sign }, {}, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });
        return shopData;
    }
}

module.exports = ShopeeAuth;