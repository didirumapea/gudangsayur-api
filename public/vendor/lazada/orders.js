const LazadaAPI = require('./index');

class LazadaOrder {

    constructor(config) {
        if (config === null || config === undefined) {
            throw new Error("config required");
        }
        //console.log('confignya: '+config);
        this.config = config;
        // INIT CLASS LAZADA API
        let paylod = {
            country: this.config.country, // if CALL API PRIVATE
            client_id: this.config.client_id,
            secret_key: this.config.secret_key,
            access_token: this.config.access_token,
            verbose: true
        }

        this.LazadaRequest = new LazadaAPI(paylod);
    }

    listOrderStatus() {
        return ['unpaid', 'pending', 'canceled', 'ready_to_ship', 'delivered', 'returned', 'shipped', 'failed'];
    }

    async getOrders(time_from, time_to, order_status) {

        // GET ORDER DATA
        let payload = {
            created_before: time_to,
            created_after: time_from,
            update_before: time_to,
            update_after: time_from,
            status: order_status,
            sort_by: 'created_at',
            sort_direction: 'DESC',
            offset: 0,
            limit: 100,
        }
        var orderData = await this.LazadaRequest.get('/orders/get', { access_token: this.config.access_token }, payload, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });
        return orderData;
    }

    async getOrderItems(order_ids) {

        // GET ORDER DATA
        let payload = {
            order_ids: "[" + order_ids + "]"
        }
        var orderData = await this.LazadaRequest.get('/orders/items/get', { access_token: this.config.access_token }, payload, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });

        return orderData;
    }

    async setStatusToPackedByMarketplace(param) {

        // GET ORDER DATA
        var orderData = await LazadaRequest.post('/order/pack', { access_token: this.config.access_token }, param, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });
        return orderData;
    }

    async getMultipleOrderItems(param) {

        // GET MULTIPLE ORDER DATA
        var orderDatas = await LazadaRequest.get('/orders/items/get', { access_token: this.config.access_token }, param, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });
        return orderDatas;

    }

    async getDocument(param) {

        // GET MULTIPLE ORDER DATA
        var orderDatas = await LazadaRequest.get('/order/document/get', { access_token: this.config.access_token }, param, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });
        return orderDatas;
    }

    async getAwbDocumentPDF(param) {

        // GET MULTIPLE ORDER DATA
        var orderDatas = await LazadaRequest.get('/order/document/awb/pdf/get', { access_token: this.config.access_token }, param, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });
        return orderDatas;
    }

    async setInvoiceNumber(param) {

        // GET MULTIPLE ORDER DATA
        var orderDatas = await LazadaRequest.post('/order/invoice_number/set', { access_token: this.config.access_token }, param, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });
        return orderDatas;
    }

    async setRepack(param) {

        // GET MULTIPLE ORDER DATA
        var orderDatas = await LazadaRequest.post('/order/repack', { access_token: this.config.access_token }, param, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });
        return orderDatas;
    }

    async setStatusToCanceled(param) {

        // GET CANCEL ORDER
        var dataCancels = await this.LazadaRequest.post('/order/cancel', { access_token: this.config.access_token }, param, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });

        return dataCancels;
    }

    async setStatusToReadyToShip(param) {

        // GET MULTIPLE ORDER DATA
        var orderDatas = await LazadaRequest.post('/order/rts', { access_token: accessToken }, param, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });
        return orderDatas;
    }

    async setStatusToSOFDelivered(param) {

        // GET MULTIPLE ORDER DATA
        var orderDatas = await LazadaRequest.post('/order/sof/delivered', { access_token: accessToken }, param, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });
        return orderDatas;
    }

    async setStatusToSOFFailedDelivery(param) {
        // GET MULTIPLE ORDER DATA
        var orderDatas = await LazadaRequest.post('/order/sof/failed_delivery', { access_token: accessToken }, param, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });
        return orderDatas;
    }
}

module.exports = LazadaOrder;