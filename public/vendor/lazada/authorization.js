const LazadaApi = require('./index');

class LazadaAuth {

    constructor(config) {
        if (config === null || config === undefined) {
            throw new Error("config required");
        }

        this.config = config;
    }

    /**
     * generateAccessToken POST /auth/token/create
     * @param {String} code
     *
     * code  // require
     */
    async generateAccessToken(code) {
        var LazadaRequest = new LazadaApi({
            client_id: this.config.client_id,
            secret_key: this.config.secret_key,
            verbose: this.config.verbose
        });

        var authData = await LazadaRequest.post('/auth/token/create', { code }, {}, function (err, res, body) {
            if (err) {
                return err;

            }
            return body;
        });

        return authData;
    }

    /**
     * refreshAccessToken POST /auth/token/refresh
     * @param {String} refresh_token
     *
     * refresh_token  // require
     */
    async refreshAccessToken(refresh_token) {
        var LazadaRequest = new LazadaApi({
            client_id: this.config.client_id,
            secret_key: this.config.secret_key,
            verbose: this.config.verbose
        });

        var authData = await LazadaRequest.post('/auth/token/refresh', {}, { refresh_token }, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });

        return authData;
    }

    /**
     * sellerInfo GET /seller/get
     *
     */
    async sellerInfo() {
        var LazadaRequest = new LazadaApi({
            country: this.config.country,
            client_id: this.config.client_id,
            secret_key: this.config.secret_key,
            verbose: this.config.verbose
        });

        var shopData = await LazadaRequest.get('/seller/get', { access_token: this.config.access_token, }, {}, function (err, res, body) {
            if (err) {
                return err;
            }
            return body;
        });

        return shopData;
    }
}

module.exports = LazadaAuth;