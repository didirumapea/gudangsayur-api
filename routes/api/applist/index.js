const router = require('express').Router();

// init controller
const {
    Marketplace,
    GetApplist,
    PostApplist,
} = require('~/src/controllers/applist/applistController');

// get merketplace
router.route('/')
    .post(PostApplist)
    .get(GetApplist);


// get merketplace
router.route('/:marketplace')
    .post(Marketplace);


module.exports = router;