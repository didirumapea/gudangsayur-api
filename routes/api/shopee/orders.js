const express = require('express');
const router = express.Router();
const authMiddleware = require('~/src/middleware/check-auth')

// init controller
const {
    getOrders,
    getDetailOrder,
    getShipmentList,
    splitOrder,
    unsplitOrder,
    cancelOrder,
    setNote,
    handleBuyerCancellation,
    addInvoiceData,
    getPendingBuyerInvoiceOrder,
    getShippingParameter,
    shipOrder,
    getTrackingNumber,
} = require('~/src/controllers/shopee/orderController');


// GET ORDER
router.route('/get-order')
    .get(authMiddleware, getOrders);

// GET ORDER DETAIL
router.route('/get-order-detail')
    .get(authMiddleware, getDetailOrder);

// GET SHIPMENT LIST
router.route('/get-shipment-list')
    .get(authMiddleware, getShipmentList);

// SPLIT ORDER
router.route('/split-order')
    .post(authMiddleware, splitOrder);

// UNSPLIT ORDER
router.route('/unsplit-order')
    .post(authMiddleware, unsplitOrder);

// CANCEL ORDER
router.route('/cancel-order')
    .post(authMiddleware, cancelOrder);

// SET NOTE
router.route('/set-note')
    .post(authMiddleware, setNote);

// HANDLE BUYER CANCELLATION
router.route('/handle-buyer-cancellation')
    .post(authMiddleware, handleBuyerCancellation);

// ADD INVOICE DATA
router.route('/add-invoice-data')
    .post(authMiddleware, addInvoiceData);

// PENDING BUYER INVOICE ORDER LIST
router.route('/pending-buyer-invoice-order')
    .get(authMiddleware, getPendingBuyerInvoiceOrder);

// GET SHIPPING PARAMETER
router.route('/shipping-parameter')
    .get(authMiddleware, getShippingParameter);

// SHIP ORDER
router.route('/ship-order')
    .post(authMiddleware, shipOrder);

// GET TRACKING NUMBER
router.route('/tracking-number')
    .get(authMiddleware, getTrackingNumber);


module.exports = router;