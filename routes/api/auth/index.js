const express = require('express');
const router = express.Router();

// init controller
const {
    Login,
    Register,
    EmailVerify,
    VerificationCode,
    ValidationCode,
    ForgotPassword,
    RegisterResendEmailVerification,
} = require('~/src/controllers/auth/authController');

// login user
router.route('/login')
    .post(Login);

// register user
router.route('/register')
    .post(Register);

// verify email register
router.route('/email/verify')
    .post(EmailVerify);

// resend email register if not receive
router.route('/register/resend-email-verification')
    .post(RegisterResendEmailVerification);

// request generate 6 code pin
router.route('/verification_code')
    .post(VerificationCode);

// validate  6 code pin
router.route('/validation_code')
    .post(ValidationCode);

// forgot password
router.route('/forgot_password')
    .post(ForgotPassword);

module.exports = router;