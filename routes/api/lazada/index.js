const router = require('express').Router();
const auth = require('~/src/middleware/check-auth');

// init controller
const {
    GenerateOauth,
    PostCallback,
    GetCallback,
    refreshAccessToken
} = require('~/src/controllers/lazada/integrationShopController');

// CONTROLLER LAZADA 

const {
    GetOrders,
    GetOrderItems,
    GetDocument,
    GetMultipleOrderItems,
    GetAwbDocumentPDF,
    SetInvoiceNumber,
    SetRepack,
    SetStatusToPackedByMarketplace,
    SetStatusToCanceled,
    SetStatusToReadyToShip,
    SetStatusToSOFDelivered,
    SetStatusToSOFFailedDelivery,

} = require('~/src/controllers/lazada/orderController');

// generate link oauth
router.route('/oauth')
    .get(GenerateOauth);

// generate link oauth
router.route('/callback')
    .post(PostCallback)
    .get(GetCallback);

// refresh access token
router.route('/refreshtoken')
    .post(auth, refreshAccessToken);

// generate link orders ===========================================================================
// route siklus ===================================================================================
router.route('/orders/get')
    .get(GetOrders);

router.route('/order/items/get')
    .get(GetOrderItems)

router.route('/order/pack')
    .post(SetStatusToPackedByMarketplace);

//=================================================================================================

router.route('/orders/items/get')
    .get(GetMultipleOrderItems);

router.route('/order/document/awb/pdf/get')
    .get(GetAwbDocumentPDF);

router.route('/order/invoice_number/set')
    .post(SetInvoiceNumber);

router.route('/order/repack')
    .post(SetRepack);

router.route('/order/document/get')
    .get(GetDocument);

router.route('/order/cancel')
    .post(SetStatusToCanceled);

router.route('/order/rts')
    .post(SetStatusToReadyToShip);

router.route('/order/sof/delivered')
    .post(SetStatusToSOFDelivered);


router.route('/order/sof/failed_delivery')
    .post(SetStatusToSOFFailedDelivery);
// =================================================================================================

module.exports = router;