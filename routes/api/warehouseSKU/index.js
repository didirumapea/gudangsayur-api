const express = require('express');
const router = express.Router();
const auth = require('~/src/middleware/check-auth');

// init controller
const {
    listWarehouseSKU,
    createWarehouseSKU,
    updateWarehouseSKU,
    deleteWarehouseSKU
} = require('~/src/controllers/warehouseSKU/warehouseSKUController');

router.route('/')
    .get(auth, listWarehouseSKU) // list SKU
    .post(auth, createWarehouseSKU); // insert SKU

router.route('/:id')
    .patch(auth, updateWarehouseSKU); // update SKU

router.route('/:id')
    .delete(auth, deleteWarehouseSKU); // delete SKU

module.exports = router;