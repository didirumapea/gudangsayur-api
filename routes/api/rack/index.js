const express = require('express');
const router = express.Router();
const auth = require('~/src/middleware/check-auth');

// init controller
const {
    listRack,
    createRack,
    updateRack,
    deleteRack
} = require('~/src/controllers/rack/rackController');

router.route('/')
    .get(auth, listRack) // list Rack
    .post(auth, createRack); // insert Rack

router.route('/:id')
    .patch(auth, updateRack); // update Rack

router.route('/:id')
    .delete(auth, deleteRack); // delete Rack

module.exports = router;