const express = require('express');
const router = express.Router();
const auth = require('~/src/middleware/check-auth');

// init controller
const {
    syncOrders,
} = require('~/src/controllers/orders/syncOrderController');
const {
    cancelOrders,
} = require('~/src/controllers/orders/cancelOrderController');
const {
    viewOrders,
} = require('~/src/controllers/orders/orderController');

// View Orders
router.route('/')
    .get(auth, viewOrders);

// Sync Orders
router.route('/sync')
    .post(auth, syncOrders);

// Cancel Orders
router.route('/cancel')
    .post(auth, cancelOrders);

module.exports = router;