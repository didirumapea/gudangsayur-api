const express = require('express');
const router = express.Router();
const auth = require('~/src/middleware/check-auth');

// init controller
const {
    listWarehouse,
    createWarehouse,
    updateWarehouse,
    deleteWarehouse
} = require('~/src/controllers/warehouse/warehouseController');

router.route('/')
    .get(auth, listWarehouse) // list warehouse
    .post(auth, createWarehouse); // insert warehouse

router.route('/:id')
    .patch(auth, updateWarehouse); // update warehouse

router.route('/:id')
    .delete(auth, deleteWarehouse); // delete warehouse

module.exports = router;