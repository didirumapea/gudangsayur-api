const express = require('express');
const router = express.Router();
const auth = require('~/src/middleware/check-auth');

// init controller
const {
    listSupplier,
    createSupplier,
    updateSupplier,
    deleteSupplier
} = require('~/src/controllers/supplier/supplierController');

router.route('/')
    .get(auth, listSupplier) // list supplier
    .post(auth, createSupplier); // insert supplier

router.route('/:id')
    .patch(auth, updateSupplier); // update supplier

router.route('/:id')
    .delete(auth, deleteSupplier); // delete supplier

module.exports = router;