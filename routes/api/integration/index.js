const express = require('express');
const router = express.Router();
const auth = require('~/src/middleware/check-auth');

// init controller
const {
    listMarketplace,
    getListShop,
    resyncShop,
    deleteShop
} = require('~/src/controllers/integration/integrationController');

// list marketplace
router.route('/marketplace')
    .get(auth, listMarketplace);

// list shop
router.route('/')
    .get(auth, getListShop);

// list shop
router.route('/')
    .get(auth, getListShop);

// re-sync shop
router.route('/resync')
    .post(auth, resyncShop);

// delete list 
router.route('/:id')
    .delete(auth, deleteShop);

module.exports = router;