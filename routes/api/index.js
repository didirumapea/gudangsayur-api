const express = require('express');
const router = express.Router();

// PATH FOLDER
// AUTH
const auth = require('./auth');
// SHOPEE
const shopee = require('./shopee');
const shopee_order = require('./shopee/orders');
// LAZADA
const lazada = require('./lazada');
// APPLIST
const applist = require('./applist');
// INTEGRATION
const integration = require('./integration');
// ORDERS
const orders = require('./orders');
// WAREHOUSE
const warehouse = require('./warehouse');
// RACK
const rack = require('./rack');
// WAREHOUSE SKU
const warehouseSKU = require('./warehouseSKU');
// SUPPLIER
const supplier = require('./supplier');

// PATH URL / API
// region AUTH
router.use(`/auth`, auth);
// endregion

// region APPLIST API
router.use(`/applist`, applist);
// endregion

// region SHOPEE API
router.use(`/shopee`, shopee);
router.use(`/shopee/order`, shopee_order);
// endregion

// region LAZADA API
router.use(`/lazada`, lazada);
// endregion

// region INTEGRATION API
router.use(`/integration`, integration);
// endregion

// region ORDERS API
router.use(`/orders`, orders);
// endregion

// region WAREHOUSE API
router.use(`/warehouse`, warehouse);
// endregion

// region RACK API
router.use(`/rack`, rack);
// endregion

// region WAREHOUSE SKU API
router.use(`/warehouse/sku`, warehouseSKU);
// endregion

// region SUPPLIER API
router.use(`/supplier`, supplier);
// endregion
module.exports = router;