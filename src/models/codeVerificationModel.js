const mongoose = require('mongoose');
// const mongoosePaginate = require('mongoose-paginate-v2');
// const mongooseAgregatePaginate = require('mongoose-aggregate-paginate-v2');
const CodeverificationSchema = require('~/src/databases/schema/codeVerificationSchema');

// UserSchema.plugin(mongoosePaginate);
// UserSchema.plugin(mongooseAgregatePaginate);

const CodeVerificationModel = mongoose.model('codeverification', CodeverificationSchema);

module.exports = CodeVerificationModel;
