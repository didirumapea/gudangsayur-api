const mongoose = require('mongoose');

// const mongoosePaginate = require('mongoose-paginate-v2');
// const mongooseAgregatePaginate = require('mongoose-aggregate-paginate-v2');
const ShopauthenticationSchema = require('~/src/databases/schema/shopAuthenticationSchema');

// UserSchema.plugin(mongoosePaginate);
// UserSchema.plugin(mongooseAgregatePaginate);

const ShopAuthenticationModel = mongoose.model('shopauthentication', ShopauthenticationSchema);

async function isExistShop(query) {
    const shop = await ShopAuthenticationModel.countDocuments(query);
    return shop;
}

async function createShopAuth(query) {
    const create = new ShopAuthenticationModel(query);
    return await create.save();
}

async function getShopbyId(query) {
    const shop = await ShopAuthenticationModel.findOne(query);
    return shop;
}

async function getAllShopbyUserId(query) {
    const shop = await ShopAuthenticationModel.find(query);
    return shop;
}

async function updateShopbyFilter(filter, query) {
    var doc = await ShopAuthenticationModel.findOneAndUpdate(filter, query, {
        new: true, rawResult: true
    });

    return doc;
}

async function deleteShopbyId(id) {
    // DELETE SHOP
    var doc = await ShopAuthenticationModel.deleteOne({ _id: id });
    if (doc.acknowledged && doc.deletedCount == 1) {
        return 1;
    } else {
        return 0;
    }
}

module.exports = { isExistShop, createShopAuth, getAllShopbyUserId, getShopbyId, updateShopbyFilter, deleteShopbyId };