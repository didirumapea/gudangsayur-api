const mongoose = require('mongoose');
// const mongoosePaginate = require('mongoose-paginate-v2');
// const mongooseAgregatePaginate = require('mongoose-aggregate-paginate-v2');
const UserSchema = require('~/src/databases/schema/userSchema');

// UserSchema.plugin(mongoosePaginate);
// UserSchema.plugin(mongooseAgregatePaginate);

const UserModel = mongoose.model('user', UserSchema);

module.exports = UserModel;