const mongoose = require('mongoose');

// const mongoosePaginate = require('mongoose-paginate-v2');
// const mongooseAgregatePaginate = require('mongoose-aggregate-paginate-v2');
const ApplistSchema = require('~/src/databases/schema/applistSchema');

// UserSchema.plugin(mongoosePaginate);
// UserSchema.plugin(mongooseAgregatePaginate);

const AppListModel = mongoose.model('applist', ApplistSchema);

async function getAllAppList(query) {
    const apps = await AppListModel.find({ env: process.env.NODE_ENV, ...query });
    return apps;
}

async function getApp(query) {
    const apps = await AppListModel.findOne({ env: process.env.NODE_ENV, ...query });
    return apps;
}

module.exports = { AppListModel, getAllAppList, getApp }
