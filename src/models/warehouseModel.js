const mongoose = require('mongoose');

// const mongoosePaginate = require('mongoose-paginate-v2');
// const mongooseAgregatePaginate = require('mongoose-aggregate-paginate-v2');
const WarehouseSchema = require('~/src/databases/schema/warehouseSchema');

// UserSchema.plugin(mongoosePaginate);
// UserSchema.plugin(mongooseAgregatePaginate);

const WarehouseModel = mongoose.model('warehouse', WarehouseSchema);

module.exports = WarehouseModel;