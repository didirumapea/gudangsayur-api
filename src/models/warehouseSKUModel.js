const mongoose = require('mongoose');

// const mongoosePaginate = require('mongoose-paginate-v2');
// const mongooseAgregatePaginate = require('mongoose-aggregate-paginate-v2');
const WarehouseSKUSchema = require('~/src/databases/schema/warehouseSKUSchema');

// UserSchema.plugin(mongoosePaginate);
// UserSchema.plugin(mongooseAgregatePaginate);

const WarehouseSKUModel = mongoose.model('warehouseSKU', WarehouseSKUSchema);

module.exports = WarehouseSKUModel;