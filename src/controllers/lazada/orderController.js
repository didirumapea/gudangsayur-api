const { errorHandler } = require('~/src/config/errorHandler');
const moment = require("moment");
const LazadaOrders = require('~/public/vendor/lazada/orders');

exports.GetOrders = async (req, res) => {

    let country='ID';
    var param = req.body;
    
    var OrderData;

    try {
        var Orders = new LazadaOrders({
            country
        });

            OrderData = await Orders.getOrders(param);

            if (OrderData.type!='ISV' && OrderData.type!='ISP')
            {
                res.json({
                    success: 1,
                    data: OrderData
                });
            }else{
                res.status(500).json({
                    success: 0,
                    data: OrderData
                });
            }
            

    }catch(error){
        errorHandler.UnHandler(res, error);
    }

   
}

exports.GetOrderItems = async (req, res) => {

    let country='ID';
    var param = req.body;
    
    var OrderData;

    try {
        var Orders = new LazadaOrders({
            country
        });

            OrderData = await Orders.getOrderItems(param);

            if (OrderData.type!='ISV' && OrderData.type!='ISP')
            {
                res.json({
                    success: 1,
                    data: OrderData
                });
            }else{
                res.status(500).json({
                    success: 0,
                    data: OrderData
                });
            }
            

    }catch(error){
        errorHandler.UnHandler(res, error);
    }

   
}

exports.SetStatusToPackedByMarketplace = async (req, res) => {

    let country='ID';
    var param = req.body;
    
    var OrderData;

    try {
        var Orders = new LazadaOrders({
            country
        });

            OrderData = await Orders.setStatusToPackedByMarketplace(param);

            if (OrderData.type!='ISV' && OrderData.type!='ISP')
            {
                res.json({
                    success: 1,
                    data: OrderData
                });
            }else{
                res.status(500).json({
                    success: 0,
                    data: OrderData
                });
            }
            

    }catch(error){
        errorHandler.UnHandler(res, error);
    }

   
}

exports.GetMultipleOrderItems = async (req, res) => {
    let country='ID';
    var param = req.body;
    var OrderData;

    try {
        var Orders = new LazadaOrders({
            country
        });

            OrderData = await Orders.getMultipleOrderItems(param);

            if (OrderData.type!='ISV' && OrderData.type!='ISP' )
            {
                res.json({
                    success: 1,
                    data: OrderData
                });
            }else{
                res.status(500).json({
                    success: 0,
                    data: OrderData
                });
            }
            

    }catch(error){
        errorHandler.UnHandler(res, error);
    }
}

exports.GetAwbDocumentPDF  = async (req, res) => {

    let country='ID';
    let param=req.body;
    var OrderData;

    try {
        var Orders = new LazadaOrders({
            country
        });

            OrderData = await Orders.getAwbDocumentPDF(param);
            console.log( OrderData.type);
            if (OrderData.type!='ISV' && OrderData.type!='ISP')
            {
                res.json({
                    success: 1,
                    data: OrderData
                });
            }else{
                res.status(500).json({
                    success: 0,
                    data: OrderData
                });
            }
            

    }catch(error){
        errorHandler.UnHandler(res, error);
    }
}


exports.SetInvoiceNumber  = async (req, res) => {

    let country='ID';
    let param=req.body;
    var OrderData;

    try {
        var Orders = new LazadaOrders({
            country
        });

            OrderData = await Orders.setInvoiceNumber(param);
            console.log( OrderData.type);
            if (OrderData.type!='ISV' && OrderData.type!='ISP')
            {
                res.json({
                    success: 1,
                    data: OrderData
                });
            }else{
                res.status(500).json({
                    success: 0,
                    data: OrderData
                });
            }
            

    }catch(error){
        errorHandler.UnHandler(res, error);
    }
}

exports.SetRepack  = async (req, res) => {

    let country='ID';
    let param=req.body;
    var OrderData;

    try {
        var Orders = new LazadaOrders({
            country
        });

            OrderData = await Orders.setRepack(param);
            console.log( OrderData.type);
            if (OrderData.type!='ISV' && OrderData.type!='ISP')
            {
                res.json({
                    success: 1,
                    data: OrderData
                });
            }else{
                res.status(500).json({
                    success: 0,
                    data: OrderData
                });
            }
            

    }catch(error){
        errorHandler.UnHandler(res, error);
    }
}


exports.GetDocument  = async (req, res) => {
    let country='ID';
    let param=req.body;
    var OrderData;

    try {
        var Orders = new LazadaOrders({
            country
        });

            OrderData = await Orders.getDocument(param);
            console.log( OrderData.type);
            if (OrderData.type!='ISV' && OrderData.type!='ISP')
            {
                res.json({
                    success: 1,
                    data: OrderData
                });
            }else{
                res.status(500).json({
                    success: 0,
                    data: OrderData
                });
            }
            

    }catch(error){
        errorHandler.UnHandler(res, error);
    }
}


exports.SetStatusToCanceled  = async (req, res) => {
    let country='ID';
    let param=req.body;
    var OrderData;

    try {
        var Orders = new LazadaOrders({
            country
        });

            OrderData = await Orders.setStatusToCanceled(param);
            console.log( OrderData.type);
            if (OrderData.type!='ISV' && OrderData.type!='ISP')
            {
                res.json({
                    success: 1,
                    data: OrderData
                });
            }else{
                res.status(500).json({
                    success: 0,
                    data: OrderData
                });
            }
            

    }catch(error){
        errorHandler.UnHandler(res, error);
    }
}

exports.SetStatusToReadyToShip  = async (req, res) => {
    let country='ID';
    let param=req.body;
    var OrderData;

    try {
        var Orders = new LazadaOrders({
            country
        });

            OrderData = await Orders.setStatusToReadyToShip(param);
            console.log( OrderData.type);
            if (OrderData.type!='ISV' && OrderData.type!='ISP')
            {
                res.json({
                    success: 1,
                    data: OrderData
                });
            }else{
                res.status(500).json({
                    success: 0,
                    data: OrderData
                });
            }
            

    }catch(error){
        errorHandler.UnHandler(res, error);
    }
}

exports.SetStatusToSOFDelivered  = async (req, res) => {
    let country='ID';
    let param=req.body;
    var OrderData;

    try {
        var Orders = new LazadaOrders({
            country
        });

            OrderData = await Orders.setStatusToSOFDelivered(param);
            console.log( OrderData.type);
            if (OrderData.type!='ISV' && OrderData.type!='ISP')
            {
                res.json({
                    success: 1,
                    data: OrderData
                });
            }else{
                res.status(500).json({
                    success: 0,
                    data: OrderData
                });
            }
            

    }catch(error){
        errorHandler.UnHandler(res, error);
    }
}

exports.SetStatusToSOFFailedDelivery  = async (req, res) => {
    let country='ID';
    let param=req.body;
    var OrderData;

    try {
        var Orders = new LazadaOrders({
            country
        });

            OrderData = await Orders.setStatusToSOFFailedDelivery(param);
            console.log( OrderData.type);
            if (OrderData.type!='ISV' && OrderData.type!='ISP')
            {
                res.json({
                    success: 1,
                    data: OrderData
                });
            }else{
                res.status(500).json({
                    success: 0,
                    data: OrderData
                });
            }
            

    }catch(error){
        errorHandler.UnHandler(res, error);
    }
}