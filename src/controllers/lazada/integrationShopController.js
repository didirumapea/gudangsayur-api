const { errorHandler } = require('~/src/config/errorHandler');
const moment = require("moment");
const { getApp } = require('~/src/models/appListModel');
const { isExistShop, getShopbyId, updateShopbyFilter, createShopAuth } = require('~/src/models/shopAuthenticationModel');
const LazadaApi = require('~/public/vendor/lazada/');
const LazadaAuth = require('~/public/vendor/lazada/authorization');
const jwt = require('jsonwebtoken');

// generate oauth
exports.GenerateOauth = async (req, res) => {
    try {
        let token = req.headers.authorization;
        let genRes = await generateOauth(token);

        res.status(200).json({
            success: 1,
            data: genRes
        })
    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// get callback lazada
exports.GetCallback = async (req, res) => {
    try {
        // VARIABLE 
        let gen_code = req.query.code;
        var accessToken, refreshToken, country, country_user_info, userID, expiredIn;
        var client_id, secret_key, resp_body, result;
        var obj, env;
        const token = req.query.token;
        const appsecret = process.env.SECRET_KEY;
        const userData = jwt.verify(token, appsecret);
        const marketplace = 'lazada';

        const local = await callbackAPI(gen_code);
        if (local) {

            // store to shopauthentication
            result = JSON.stringify(local);
            obj = JSON.parse(result);
            env = process.env.NODE_ENV;
            accessToken = obj.access_token;
            refreshToken = obj.refresh_token;
            country = obj.country.toLocaleUpperCase();
            country_user_info = obj.country_user_info;
            userID = userData.userID;
            expiredIn = moment().add(obj.expires_in, 'second').format('X');

            const apps = await getApp({ marketplace });
            if (apps) {
                client_id = apps.appid;
                secret_key = apps.appsecret;

                // GET SHOP DATA
                var Lazadarequest = new LazadaAuth({
                    country, // if CALL API PRIFAVATE
                    client_id,
                    secret_key,
                    access_token: accessToken,
                    verbose: true
                });
                var shopData = await Lazadarequest.sellerInfo();

                // CHECK EXIST
                var checkShop = await isExistShop({ shopID: shopData.data.seller_id, marketplace });
                if (checkShop > 0) {
                    return errorHandler.BadRequest(res, "seller's account is already linked to another user");
                }

                // CONSTRUCT DATA SHOP AUTHENTICATION
                let payload = {
                    marketplace,
                    userID,
                    shopName: shopData.data.name,
                    shopID: shopData.data.seller_id,
                    country: country,
                    accessToken: accessToken,
                    refreshToken: refreshToken,
                    expiredIn: expiredIn,
                };
                const newShopAuthentication = await createShopAuth(payload);
                res.json({
                    success: 1,
                    msg: 'shop integration successfull'
                });
            }
        }
    } catch (error) {
        errorHandler.UnHandler(res, error);
    }
}

// post callback lazada
exports.PostCallback = async (req, res) => {
    console.log(moment().format("DD-MM-YYYY HH:mm:ss"))
    console.log("body lazada : ", req.body)

    res.status(200).json({
        success: 1,
    });
}

// post refresh access token
exports.refreshAccessToken = async (req, res) => {
    try {
        var client_id, secret_key, refresh_token, request = req.body, user = req.userData;

        // AMBIL DATA DARI APPLIST
        const apps = await getApp({ marketplace: 'lazada' });
        if (apps) {

            // AMBIL SHOP DATA DARI APPLIST
            const shop = await getShopbyId({ userID: user.userID, shopID: request.shopID, marketplace: 'lazada' });

            client_id = apps.appid;
            secret_key = apps.appsecret;
            refresh_token = shop.refreshToken;

            var LazadaRequest = new LazadaAuth({
                client_id,
                secret_key,
                verbose: true
            });

            // CALL API REFRESH TOKEN
            var authData = await LazadaRequest.refreshAccessToken(refresh_token);
            let accessToken = authData.access_token;
            let refreshToken = authData.refresh_token;
            let expiredIn = moment().add(authData.expires_in, 'second').format('X');

            const filter = { userID: user.userID, shopID: request.shopID, marketplace: 'lazada' };
            const update = { accessToken, refreshToken, expiredIn };

            var doc = await updateShopbyFilter(filter, update);
        }

        res.status(200).json({
            success: 1,
            data: { doc }
        });

    } catch (error) {
        errorHandler.UnHandler(res, error);
    }
}

// FUNCTION
async function callbackAPI(gen_code) {
    // INIT VARIABLE
    let code = gen_code;
    var client_id, secret_key, authData;

    // AMBIL DATA DARI APPLIST
    const apps = await getApp({ marketplace: 'lazada' });
    if (apps) {

        client_id = apps.appid;
        secret_key = apps.appsecret;

        // GENERATE ACCESS-TOKEN
        var LazadaRequest = new LazadaAuth({
            client_id,
            secret_key,
            verbose: true
        });
        authData = LazadaRequest.generateAccessToken(code);
    }
    return authData;
}

async function buildAuthURL(token, marketplace) {

    // VARIABLE
    let redirect,
        client_id,
        url,
        url_resp;

    const apps = await getApp({ marketplace });

    if (apps) {
        // CONFIG Lazada API

        client_id = apps.appid;
        redirect = apps.apicallback + "?token=" + token;

        url = new LazadaApi({ redirect, client_id });
        url_resp = url.buildAuthURL();

    }
    return url_resp;
}

async function generateOauth(token) {

    let urlOauth = buildAuthURL(token, 'lazada');
    return urlOauth;
}