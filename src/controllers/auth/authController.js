const { errorHandler } = require('~/src/config/errorHandler');
const User = require('~/src/models/userModel');
const Codeverification = require('~/src/models/codeVerificationModel');
const config = require('~/src/config');
const { MailerService } = require('~/src/config/nodemailer');
const jwt = require('jsonwebtoken');

const securePin = require('secure-pin');
const crypto = require('crypto');
const moment = require('moment');
const {
    register
} = require('~/validators/user');

// bcrypt config
const bcrypt = require('bcrypt');
const saltRounds = 10;

// login user
exports.Login = async (req, res) => {
    try {
        var email = req.body.email;

        if (!req.body.email) {
            return errorHandler.BadRequest(res, 'Email is required');
        }
        if (!req.body.password) {
            return errorHandler.BadRequest(res, 'Password is empty');
        }

        const user = await User.findOne({ 'email': req.body.email });

        if (user) {
            //console.log(user);
            var userID = user._id;
            var emailVerifiedAt = user.emailVerifiedAt;
            var isVerified;

            if (emailVerifiedAt == null) {
                isVerified = false;
            } else if (emailVerifiedAt !== null) {
                isVerified = true;
            }

            //console.log(userID);
            bcrypt.compare(req.body.password, user.password, function (err1, res1) {

                if (res1) {
                    // generate access token

                    var accessToken = crypto.randomBytes(32).toString('Hex');
                    var sign = jwt.sign({ userID, accessToken, email }, process.env.SECRET_KEY, { expiresIn: '86400s' });

                    //update to mongo
                    User.findOneAndUpdate({ "email": email }, { "$set": { "accessToken": accessToken } }).exec(function (err, user) {
                        if (err) {
                            console.log(err);
                            //res.status(500).send(err);
                        }
                    });
                    // await User.updateOne({ email : email }, { accessToken:access_token, refreshToken:refresh_token, updatedAt:update_at }, options, callback);

                    res.json({ success: 1, data: { accessToken: sign, email: email, isVerified: isVerified } });


                } else {
                    return errorHandler.BadRequest(res, 'Your password is wrong');
                }

            })
        } else {
            return errorHandler.BadRequest(res, 'Email not registered');
        }

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }

}

// register user
exports.Register = async (req, res) => {
    try {
        if (!req.body.email) {
            return errorHandler.BadRequest(res, 'Email is required');
        }

        const user = await User.findOne({
            $or: [
                { email: req.body.email },
                { noPhone: req.body.phone }
            ]
        });
        if (user) {
            if (user.email === req.body.email) {
                return errorHandler.BadRequest(res, 'Email already taken.');
            } else if (user.noPhone === req.body.phone) {
                return errorHandler.BadRequest(res, 'Phone already registered');
            } else {
                return errorHandler.BadRequest(res, 'User is exist');
            }
        } else { // register
            const validate = register.validate(req.body);
            if (validate.error) {
                throw validate.error;
            }
            bcrypt.genSalt(saltRounds, (err, salt) => {
                if (err) throw err;

                bcrypt.hash(req.body.password, salt, async (err, hash) => {
                    if (err) throw err;

                    const resData = {
                        accessToken: crypto.randomBytes(32).toString('Hex'),
                        email: req.body.email
                    }
                    // const accessToken =  crypto.randomBytes(32).toString('Hex');
                    const newUser = new User({
                        name: req.body.fullname,
                        email: resData.email,
                        password: hash,
                        noPhone: req.body.phone,
                        accessToken: resData.accessToken,
                    });
                    await newUser.save();

                    const user = await User.findOne({ email: req.body.email });
                    const expired = moment().add('30', 'minutes').unix();
                    const payload = payloadJwt(user);
                    const sign = jwt.sign(payload, config.jwtSecretKey, { expiresIn: '1800s' });
                    resData.isVerified = !!user.emailVerifiedAt;
                    resData.accessToken = sign;
                    MailerService({
                        message_title: 'Pendaftaran Baru',
                        from: process.env.EMAIL_SENDER_SES, // email sender
                        to: resData.email, // resData.email
                        layoutTemplate: 'auth/register',
                        subject: 'Monga - Pendaftaran Baru',
                        context: {
                            url: process.env.API_URL + `auth/email/verify?t=${sign}&e=${expired}`
                        }
                    });
                    res.json({
                        success: 1,
                        data: resData
                    });
                });
            });
        }
    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// verify email register
exports.EmailVerify = async (req, res) => {
    const token = req.query.t;

    try {
        const decodeSign = jwt.verify(token, config.jwtSecretKey);
        const user = await User.findOne({ email: decodeSign.email });

        if (decodeSign.accessToken === user.accessToken) {
            await User.findOneAndUpdate({ email: decodeSign.email }, { emailVerifiedAt: moment().unix() }, {
                new: true,
                upsert: true
            });
            res.json({
                success: 1,
                msg: 'your account is verified'
            })
        } else {
            res.json({
                success: 0,
                msg: 'Invalid url, please request again'
            })
        }

    } catch (error) {
        errorHandler.UnHandler(res, error);
    }
}

// request generate 6 code pin
exports.VerificationCode = async (req, res) => {
    if (!req.body.email) {
        return errorHandler.BadRequest(res, 'Email is required');
    }

    const user = await User.findOne({ 'email': req.body.email });


    if (user) {
        //console.log(user);
        var pin = securePin.generatePinSync(6);
        console.log(pin);
        //const expired = moment().add('30', 'minutes').unix();

        const newCode = new Codeverification({
            email: req.body.email,
            code: pin,
            // verification_code: randomNumber,
        });
        await newCode.save();

        // send email
        MailerService({
            message_title: 'Verification Code',
            from: process.env.EMAIL_SENDER_SES, // email sender
            to: req.body.email,
            layoutTemplate: 'auth/verification_code',
            subject: 'Monga - Verification Code',
            context: {
                pin: pin
            }
        });



        res.json({
            success: 1,
            msg: `Please check your email`
        });


    } else {
        return errorHandler.BadRequest(res, 'Email not registered');
    }
}

// validate  6 code pin
exports.ValidationCode = async (req, res) => {
    if (!req.body.email) {
        return errorHandler.BadRequest(res, 'Email is required');
    }

    if (!req.body.code) {
        return errorHandler.BadRequest(res, 'Code is required');
    }

    const user = await User.findOne({ 'email': req.body.email });
    var email = req.body.email;
    var pin = req.body.code
    //console.log(email);
    //console.log(pin);

    if (user) {
        const code = await Codeverification.findOne({ 'email': email, 'code': pin });
        //console.log(code);

        if (code) {
            let createdAt = code.createdAt;
            console.log(createdAt);
            let now = moment().unix();
            console.log(now);

            var start = moment.unix(createdAt);
            var end = moment.unix(now);

            var diff = moment.duration(end.diff(start)).minutes();

            if (diff <= 30) {
                Codeverification.deleteOne({ 'email': email, 'code': pin }).then(function () {
                    console.log("Data deleted");
                });

                res.json({ success: 1, msg: 'code is valid' });
            } else {

                Codeverification.deleteOne({ 'email': email, 'code': pin }).then(function () {
                    console.log("Data deleted");
                });

                return errorHandler.BadRequest(res, 'Code is invalid');
            }

        } else {
            return errorHandler.BadRequest(res, 'Code not found');
        }
    } else {
        return errorHandler.BadRequest(res, 'Email not found');
    }
}

// forgot password
exports.ForgotPassword = async (req, res) => {
    if (!req.body.email) {
        return errorHandler.BadRequest(res, 'Email is required');
    }

    if (!req.body.code) {
        return errorHandler.BadRequest(res, 'Code is required');
    }

    if (!req.body.newPassword) {
        return errorHandler.BadRequest(res, 'New password is required');
    }

    const user = await User.findOne({ 'email': req.body.email });

    let email = req.body.email;
    let pin = req.body.code;
    let isValid = false;

    if (user) {

        //check code at verification code
        const code = await Codeverification.findOne({ 'email': email, 'code': pin });
        if (code) {
            let createdAt = code.createdAt;
            console.log(createdAt);
            let now = moment().unix();
            console.log(now);

            var start = moment.unix(createdAt);
            var end = moment.unix(now);

            var diff = moment.duration(end.diff(start)).minutes();

            console.log('here: ' + diff);

            if (diff <= 10) {
                isValid = true;

                // valid
                isValid = true;

                bcrypt.genSalt(saltRounds, (err, salt) => {
                    if (err) throw err;

                    bcrypt.hash(req.body.newPassword, salt, async (err, hash) => {
                        if (err) throw err;
                        // update password
                        User.findOneAndUpdate({ "email": email }, { "$set": { "password": hash } }).exec(function (err, user) {
                            if (err) {
                                console.log(err);
                            }

                            // delete code verification
                            Codeverification.deleteOne({ 'email': email, 'code': pin }).then(function () {
                                console.log("Data deleted");
                                // Success
                                res.json({
                                    success: 1,
                                    msg: `password changes successfully`
                                });

                            }).catch(function (error) {
                                console.log(error); // Failure
                            });

                        });

                    })



                })

            } else {
                isValid = false;
                //delete code verification
                Codeverification.deleteOne({ 'email': email, 'code': pin }).then(function () {
                    console.log("Data deleted"); // Success

                    return errorHandler.BadRequest(res, 'invalid code');

                }).catch(function (error) {
                    //console.log(error); // Failure
                });
            }


        } else {
            return errorHandler.BadRequest(res, 'invalid code');
        }
    } else {
        return errorHandler.BadRequest(res, 'Email not found');
    }
}

// resend email register if not receive
exports.RegisterResendEmailVerification = async (req, res) => {
    try {
        const resData = {
            accessToken: crypto.randomBytes(32).toString('Hex'),
            email: req.body.email
        }
        const user = await User.findOne({ email: req.body.email });
        if (user) {
            const expired = moment().add('30', 'minutes').unix();
            const payload = payloadJwt(user);
            const sign = jwt.sign(payload, config.jwtSecretKey, { expiresIn: '1800s' });
            resData.isVerified = !!user.emailVerifiedAt;
            MailerService({
                message_title: 'Pendaftaran Baru',
                from: process.env.EMAIL_SENDER_SES, // email sender
                to: resData.email, // resData.email
                layoutTemplate: 'auth/register',
                subject: 'Monga - Pendaftaran Baru',
                context: {
                    url: process.env.API_URL + `web/auth/email/verify?t=${sign}&e=${expired}`
                }
            });
            res.json({
                success: 1,
                data: 'please check your email'
            });
        } else {
            errorHandler.UnHandler(res, 'User Not Found');
        }


    } catch (error) {
        errorHandler.UnHandler(res, error);
    }
}

const payloadJwt = (user) => ({
    _id: user._id,
    email: user.email,
    accessToken: user.accessToken,
});