const { errorHandler } = require('~/src/config/errorHandler');
const SupplierModel = require('~/src/models/supplierModel');

exports.listSupplier = async (req, res) => {
    // VARIABLE
    const user = req.userData;
    var request = req.body;

    // DATA SUPPLIER
    let payload = {
        userID: user.userID,
        ...request
    }

    var supplierData = await SupplierModel.find(payload);
    if (!supplierData) {
        return errorHandler.NotFound(res, "Data not found");
    }

    res.status(200).json({
        success: 1,
        data: supplierData
    });
}

exports.createSupplier = async (req, res) => {
    // VARIABLE
    const user = req.userData;
    var request = req.body;
    var supplierData = [];

    // STORE SUPPLIER
    let payload = {
        userID: user.userID,
        ...request
    }
    try {
        const newSupplier = new SupplierModel(payload);
        await newSupplier.save();

        // GET SUPPLIER DATA
        var supplierData = await SupplierModel.find({ userID: user.userID });
        if (!supplierData) {
            return errorHandler.NotFound(res, "Data not found");
        }

    } catch (error) {
        let err = error.message;
        return errorHandler.BadRequest(res, err);
    }

    res.status(200).json({
        success: 1,
        data: supplierData
    });
}

exports.updateSupplier = async (req, res) => {
    // VARIABLE
    const user = req.userData;
    var params = req.params;
    var request = req.body;
    var supplierData = [];

    // UPDATE DOC COLLECTION
    const filter = { userID: user.userID, _id: params.id },
        update = request;
    var doc = await SupplierModel.findOneAndUpdate(filter, update, {
        new: true, rawResult: true
    });

    if (!doc.lastErrorObject.updatedExisting) {
        return errorHandler.BadRequest(res, "Failed to update data.");
    }

    // GET SUPPLIER DATA
    var supplierData = await SupplierModel.find({ userID: user.userID });
    if (!supplierData) {
        return errorHandler.NotFound(res, "Data not found");
    }

    res.status(200).json({
        success: 1,
        data: supplierData
    });
}

exports.deleteSupplier = async (req, res) => {

    try {
        // VARIABLE
        var request = req.params;
        let user = req.userData;
        var supplierData = [];

        // DELETE SUPPLIER
        var doc = await SupplierModel.deleteOne({ _id: request.id });
        if (doc.acknowledged && doc.deletedCount == 1) {

            // GET SUPPLIER DATA
            var supplierData = await SupplierModel.find({ userID: user.userID });
            if (!supplierData) {
                return errorHandler.NotFound(res, "Data not found");
            }

            res.status(200).json({
                success: 1,
                data: supplierData
            });

        } else {
            return errorHandler.BadRequest(res, "Record doesn't exist or already deleted")
        }

    } catch (error) {
        return errorHandler.UnHandler(res, error)
    }
}