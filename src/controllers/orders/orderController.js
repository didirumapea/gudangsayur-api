const { errorHandler } = require('~/src/config/errorHandler');
const moment = require("moment");
const { getApp } = require('~/src/models/appListModel');
const { getShopbyId } = require('~/src/models/shopAuthenticationModel');
const { getAllOrders } = require('~/src/models/ordersModel');

exports.viewOrders = async (req, res) => {
    try {
        // GET DATA ORDERS
        var orders = await getAllOrders();
        res.status(200).json({
            success: 1,
            data: orders
        });
    } catch (error) {
        return errorHandler.UnHandler(res, error)
    }
}