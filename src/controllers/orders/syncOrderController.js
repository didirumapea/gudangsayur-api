const { errorHandler } = require('~/src/config/errorHandler');
const moment = require("moment");
const { getApp } = require('~/src/models/appListModel');
const { getShopbyId } = require('~/src/models/shopAuthenticationModel');
const { createOrders, getLastCreate } = require('~/src/models/ordersModel');
const ShopeeOrder = require('~/public/vendor/shopee/orders');
const LazadaOrder = require('~/public/vendor/lazada/orders');

exports.syncOrders = async (req, res) => {
    try {
        // VARIABLE
        var request = req.body,
            dataOrders = [],
            orders;

        if (request.sync == "all") {
            dataOrders = await getAllOrder();
        }

        if (request.sync != "all") {
            for await (const iterator of request.sync) {
                let marketplace = iterator.marketplace;
                let shopID = iterator.shopID;

                switch (marketplace) {
                    case "shopee":
                        orders = await getOrdersShopee(shopID, req);
                        break;
                    case "lazada":
                        orders = await getOrdersLazada(shopID, req);
                        break;

                    default:
                        return errorHandler.BadRequest(res, "marketplace not found");
                }

                dataOrders.push(orders);
            }
        }

        res.status(200).json({
            success: 1,
            data: dataOrders
        });

    } catch (error) {
        return errorHandler.UnHandler(res, error)
    }
}

async function getAllOrder() {

}

async function getOrdersShopee(shopID, req) {
    //  VARIABLE
    const user = req.userData, marketplace = 'shopee';
    var dataOrderSN = [],
        dataOrderRequest = [],
        dataOrderResponse = [],
        countNewData = 0,
        time_from, time_to;

    // AMBIL DATA DARI APPLIST
    const apps = await getApp({ marketplace });
    if (!apps) {
        return { error: "error_not_found", message: "Shop not found" };
    }

    // AMBIL SHOP DATA DARI SHOP-AUTH
    const shop = await getShopbyId({ shopID, marketplace });
    if (!shop) {
        return { error: "error_not_found", message: "Shop not found" };
    }

    var shopeeRequest = new ShopeeOrder({
        partner_id: apps.appid,
        partner_key: apps.appsecret,
        shop_id: parseInt(shop.shopID),
        access_token: shop.accessToken
    })

    // CHECK LAST CREATE ORDERS
    let lastCreate = await getLastCreate({ shopID: shop._id });
    if (lastCreate == null) {
        time_from = moment().subtract(24, 'hour').format("X"); // default 24 lasthour;
    } else {
        time_from = lastCreate.createdAt
    }
    time_to = moment().unix(); //default now()

    // GET ORDERS
    var statusOrder = shopeeRequest.listOrderStatus();
    for await (const order of statusOrder) {
        var order_status = order;

        var shopeeResponse = await shopeeRequest.getOrders(time_from, time_to, order_status);
        if (shopeeResponse.error != '') {
            continue;
        }

        var order_sn_list = shopeeResponse.response.order_list;
        order_sn_list.forEach(element => {
            dataOrderSN.push(element.order_sn)
        });
        sleep(500);
    }

    // CONSTRUCT TO STRING DAN TAMP DI ARRAY MAKS STRING 50 ORDER SN
    let length = dataOrderSN.length;
    var orderSN = "";
    for (let i = 0; i < length; i++) {
        orderSN += dataOrderSN[i] + ",";
        if ((i + 1) == length) {
            dataOrderRequest.push(orderSN);
            break;
        }
        if ((i + 1) % 50 == 0) {
            dataOrderRequest.push(orderSN);
            orderSN = "";
        }
    }

    // GET MULTI DETAIL ORDER
    for await (const order_sn_list of dataOrderRequest) {
        var shopeeDetails = await shopeeRequest.getDetailOrder(order_sn_list);
        // console.log(shopeeDetails)
        if (shopeeDetails.error != "") {
            return { error: '', message: shopeeDetails.message };
        }
        dataOrderResponse = shopeeDetails.response.order_list;

        // INSERT COLLECTION
        for await (const order_response of dataOrderResponse) {
            let payload = {
                userID: user.userID,
                shopID: shop._id,
                marketplace,
                status: "pending_proccess",
                items: order_response,
            }

            let create = await createOrders(payload);
            countNewData++;
        }
        sleep(500);
    }

    return `${shop.shopName} has ${countNewData} new orders.`;
}

async function getOrdersLazada(shopID, req) {

    //  VARIABLE
    const user = req.userData, marketplace = 'lazada';
    var dataOrderSN = [],
        dataOrderRequest = [],
        dataOrderResponse = [],
        countNewData = 0,
        time_from, time_to;

    // AMBIL DATA DARI APPLIST
    const apps = await getApp({ marketplace });
    if (!apps) {
        return { code: 2, message: "Shop not found" };
    }

    // AMBIL SHOP DATA DARI SHOP-AUTH
    const shop = await getShopbyId({ shopID, marketplace });
    if (!shop) {
        return { code: 2, message: "Shop not found" };
    }

    var lazadaRequest = new LazadaOrder({
        country: 'ID', // if CALL API PRIVATE
        client_id: apps.appid,
        secret_key: apps.appsecret,
        access_token: shop.accessToken,
        verbose: true
    });
    var statusOrder = lazadaRequest.listOrderStatus();

    // CHECK LAST CREATE ORDERS
    let lastCreate = await getLastCreate({ shopID: shop._id });
    if (lastCreate == null) {
        time_from = moment().subtract(24, 'hour').format(); // default 24 lasthours;
    } else {
        time_from = lastCreate.createdAt
    }
    time_to = moment().format(); //default now()

    // GET ORDERS
    for await (const status of statusOrder) {
        let order_status = status;

        var lazadaOrders = await lazadaRequest.getOrders(time_from, time_to, order_status);
        if (lazadaOrders.code != 0) {
            continue;
        }

        var order_sn_list = lazadaOrders.data.orders;
        order_sn_list.forEach(element => {
            dataOrderSN.push(element.order_id)
        });

        sleep(500);
    }

    // CONSTRUCT TO STRING DAN TAMP DI ARRAY MAKS STRING 100 ORDER SN
    let length = dataOrderSN.length;
    var orderSN = "";
    for (let i = 0; i < length; i++) {
        orderSN += dataOrderSN[i] + ",";
        if ((i + 1) == length) {
            dataOrderRequest.push(orderSN);
            break;
        }
        if ((i + 1) % 100 == 0) {
            dataOrderRequest.push(orderSN);
            orderSN = "";
        }
    }

    // GET MULTI DETAIL ORDER
    for await (const order_ids of dataOrderRequest) {
        var lazadaDetails = await lazadaRequest.getOrderItems(order_ids);
        if (lazadaDetails.code != 0) {
            return { error: '', message: lazadaDetails.message };
        }
        dataOrderResponse = lazadaDetails.data;

        // INSERT COLLECTION
        for await (const order_response of dataOrderResponse) {
            let payload = {
                userID: user.userID,
                shopID: shop._id,
                marketplace,
                status: "pending_proccess",
                items: order_response,
            }

            let create = await createOrders(payload);
            countNewData++;
        }

        sleep(500);
    }

    return `${shop.shopName} has ${countNewData} new orders.`;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}