const { errorHandler } = require('~/src/config/errorHandler');
const ShopeeOrder = require('~/public/vendor/shopee/orders');
const LazadaOrder = require('~/public/vendor/lazada/orders');
const { getApp } = require('~/src/models/appListModel');
const { getShopbyId } = require('~/src/models/shopAuthenticationModel');

exports.cancelOrders = async (req, res) => {
    try {
        // VARIABLE
        var request = req.body;
        const user = req.userData;
        var requestCancel = request.order;
        var dataCancel = [], response;

        // LOOP DATA
        for await (const iterator of requestCancel) {
            let marketplace = iterator.marketplace;

            switch (marketplace) {
                case "shopee":
                    response = await cancelOrdersShopee(iterator);
                    break;
                case "lazada":
                    response = await cancelOrdersLazada(iterator)
                    break;
                default:
                    break;
            }

            // BUILD RESPONSE

            dataCancel.push(response);
        }

        res.status(200).json({
            success: 1,
            data: dataCancel
        });
    } catch (error) {
        return errorHandler.BadRequest(res, error);
    }
}

async function cancelOrdersShopee(paramCancel) {

    // AMBIL DATA DARI APPLIST
    const apps = await getApp({ marketplace: 'shopee' });
    if (!apps) {
        return { error: "data_not_found", message: "Platform not found" };
    }

    // AMBIL SHOP DATA DARI SHOP-AUTH
    const shop = await getShopbyId({ shopID: paramCancel.shopID, marketplace: 'shopee' });
    if (!shop) {
        return { error: "data_not_found", message: "Shop not found" };
    }

    // REQ CANCEL ORDERS
    var shopeeRequest = new ShopeeOrder({
        partner_id: apps.appid,
        partner_key: apps.appsecret,
        shop_id: parseInt(shop.shopID),
        access_token: shop.accessToken
    })

    // FIND DATA ORDER FROM COLLECTION

    // CONSTRUCT REQUEST
    let payload = {
        order_sn: "201020SQQ5K2EP",
        cancel_reason: "OUT_OF_STOCK",
        item_list: [
            {
                item_id: 1680783,
                model_id: 327890123
            }
        ]
    }
    let detailOrder = await shopeeRequest.cancelOrder(payload);

    return detailOrder;
}

async function cancelOrdersLazada(paramCancel) {
    // AMBIL DATA DARI APPLIST
    const apps = await getApp({ marketplace: 'lazada' });
    if (!apps) {
        return { code: 1, message: "Platform not found" };
    }

    // AMBIL SHOP DATA DARI SHOP-AUTH
    const shop = await getShopbyId({ shopID: paramCancel.shopID, marketplace: 'lazada' });
    if (!shop) {
        return { code: 1, message: "Shop not found" };
    }

    // REQ CANCEL ORDERS
    var lazadaRequest = new LazadaOrder({
        shop_id: parseInt(shop.shopID),
        access_token: shop.accessToken,
        country: 'ID', // if CALL API PRIFAVATE
        client_id: apps.appid,
        secret_key: apps.appsecret,
    })

    // FIND DATA ORDER FROM COLLECTION


    // CONSTRUCT REQUEST
    let payload = {
        reason_detail: "Out of stock",
        reason_id: 15,
        order_item_id: 140168
    }
    let detailOrder = await lazadaRequest.setStatusToCanceled(payload);

    return detailOrder;
}