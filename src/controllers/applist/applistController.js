const { errorHandler } = require('~/src/config/errorHandler');
const Applist = require('~/src/models/appListModel');

// get applist
exports.GetApplist = async (req, res) => {
    Applist.find({}, function(err, apps) {
        res.json({
            success: 1,
            data: apps
        });
    })
}

// post applist
exports.PostApplist = async (req, res) => {
    try {
        if (!req.body.marketplace) {
            return errorHandler.BadRequest(res, 'Marketplace is required');
        }

        if (!req.body.appid) {
            return errorHandler.BadRequest(res, 'AppID is required');
        }

        if (!req.body.appsecret) {
            return errorHandler.BadRequest(res, 'AppSecret is required');
        }

        if (!req.body.apicallback) {
            return errorHandler.BadRequest(res, 'Api Callback is required');
        }

        if (!req.body.apiwebhook) {
            return errorHandler.BadRequest(res, 'Api Webhook is required');
        }

        const newApplist = new Applist({
            marketplace: req.body.marketplace,
            appid: req.body.appid,
            appsecret: req.body.appsecret,
            apicallback:  process.env.API_REDIRECT + req.body.marketplace +'/'+req.body.apicallback,
            apiwebhook:  process.env.API_REDIRECT + req.body.marketplace +'/'+req.body.apiwebhook,
            description:req.body.description
        });
        await newApplist.save();
        res.json({
            success: 1,
            msg: 'Applist saved'
        });

    }catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// marketplace
exports.Marketplace = async (req, res) => {
    try {
        Applist.find({ 'marketplace': req.params.marketplace }, function (err, apps) {
            if (err) return handleError(err);
            res.json({
                success: 1,
                data: apps
            });
        })

    }catch(error){
        errorHandler.UnHandler(res, error);
    }
}
