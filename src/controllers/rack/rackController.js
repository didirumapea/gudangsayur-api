const { errorHandler } = require('~/src/config/errorHandler');
const RackModel = require('~/src/models/rackModel');

exports.listRack = async (req, res) => {

    // VARIABLE
    const user = req.userData;
    var request = req.body;
    var rackData = [];

    // DATA RACK
    let payload = {
        userID: user.userID,
        ...request
    }

    rackData = await RackModel.find(payload);
    if (!rackData) {
        return errorHandler.NotFound(res, "Data not found");
    }

    res.status(200).json({
        success: 1,
        data: rackData
    });
}

exports.createRack = async (req, res) => {
    // VARIABLE
    const user = req.userData;
    var request = req.body;
    var rackData = [];

    // STORE RACK
    let payload = {
        userID: user.userID,
        ...request
    }

    try {
        const newRack = new RackModel(payload);
        await newRack.save();

        // GET RACK DATA
        var rackData = await RackModel.find({ userID: user.userID });
        if (!rackData) {
            return errorHandler.NotFound(res, "Data not found");
        }

    } catch (error) {
        let err = error.message;
        return errorHandler.BadRequest(res, err);
    }

    res.status(200).json({
        success: 1,
        data: rackData
    });
}

exports.updateRack = async (req, res) => {
    // VARIABLE
    const user = req.userData;
    var params = req.params;
    var request = req.body;
    var rackData = [];

    // UPDATE DOC COLLECTION
    const filter = { userID: user.userID, _id: params.id },
        update = request;
    var doc = await RackModel.findOneAndUpdate(filter, update, {
        new: true, rawResult: true
    });
    if (!doc.lastErrorObject.updatedExisting) {
        return errorHandler.BadRequest(res, "Failed to update data.");
    }

    // GET RACK DATA
    var rackData = await RackModel.find({ userID: user.userID });
    if (!rackData) {
        return errorHandler.NotFound(res, "Data not found");
    }
    res.status(200).json({
        success: 1,
        data: rackData
    });
}

exports.deleteRack = async (req, res) => {
    try {
        // VARIABLE
        var request = req.params;
        let user = req.userData;
        var rackData = [];

        // DELETE RACK
        var doc = await RackModel.deleteOne({ _id: request.id });
        if (doc.acknowledged && doc.deletedCount == 1) {

            // GET RACK DATA
            rackData = await RackModel.find({ userID: user.userID });
            if (!rackData) {
                return errorHandler.NotFound(res, "Data not found");
            }

            res.status(200).json({
                success: 1,
                data: rackData
            });

        } else {
            return errorHandler.BadRequest(res, "Record doesn't exist or already deleted")
        }

    } catch (error) {
        return errorHandler.UnHandler(res, error)
    }
}