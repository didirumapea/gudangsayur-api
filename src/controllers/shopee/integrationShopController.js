const { errorHandler } = require('~/src/config/errorHandler');
const { getApp } = require('~/src/models/appListModel');
const { createShopAuth, isExistShop } = require('~/src/models/shopAuthenticationModel');
const ShopeeAuth = require('~/public/vendor/shopee/authorization');
const jwt = require('jsonwebtoken');
const moment = require("moment");

// callback shopee
exports.Callback = async (req, res) => {

    try {

        // INITIAL VARIABLE
        const code = req.query.code;
        const shopID = req.query.shop_id;
        const token = req.query.token;
        const appsecret = process.env.SECRET_KEY;
        const userData = jwt.verify(token, appsecret);
        const marketplace = "shopee";

        // FIND APP LIST
        const appData = await getApp({ marketplace });

        // GENERATE AUTH-DATA
        let shopeeRequest = new ShopeeAuth({
            partner_id: appData.appid,
            partner_key: appData.appsecret
        });

        let authData = await shopeeRequest.generateAccessToken(code, shopID);
        if (authData.error != '') {
            return errorHandler.BadRequest(res, authData.message ? authData.message : authData.msg);
        }

        // GET INFO-SHOP
        shopeeRequest = new ShopeeAuth({
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: authData.access_token,
            shop_id: shopID,
        });
        let shopData = await shopeeRequest.sellerInfo();
        if (authData.error != '') {
            return errorHandler.BadRequest(res, authData.message ? authData.message : authData.msg);
        }

        // CHECK EXIST
        var checkShop = await isExistShop({ shopID, marketplace });
        if (checkShop > 0) {
            return errorHandler.BadRequest(res, "seller's account is already linked to another user");
        }

        // CONSTRUCT DATA SHOP AUTHENTICATION
        let payload = {
            userID: userData.userID,
            marketplace,
            shopID: shopID,
            shopName: shopData.shop_name,
            country: shopData.region,
            accessToken: authData.access_token,
            refreshToken: authData.refresh_token,
            expiredIn: shopData.expire_time
        }
        const newShopAuthentication = await createShopAuth(payload);

        res.status(200).json({
            success: 1,
            msg: 'shop integration successful'
        });
    } catch (error) {
        errorHandler.UnHandler(res, error);
    }
}

// callback shopee
exports.WebHook = async (req, res) => {
    console.log(moment().format("DD-MM-YYYY HH:mm:ss"))
    console.log("body shopee : ", req.body)

    res.status(200).json({
        success: 1
    });

}