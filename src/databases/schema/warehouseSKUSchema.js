const mongoose = require('mongoose');
const timestamps = require('mongoose-unix-timestamp-plugin');
const { Schema } = mongoose;

const WarehouseSKU = new Schema({
    userID: { type: Schema.Types.ObjectId, ref: "user" },
    image: { type: String, default: null },
    numberSKU: { type: String, required: true, unique: true, },
    combinationSKU: { type: Array, default: null },
    shopSKU: [{ shopName: String, productSKU: String }],
    title: { type: String, required: true },
    category: { type: String, default: null },
    gtin: { type: String, default: null },
    referencePrice: { type: Number, default: 0 },
    productDimensions: {
        weight: Number,
        length: Number,
        width: Number,
        height: Number
    },
    stock: {
        total: Number,
        avaliable: Number,
        allocated: Number,
        promoReserved: Number,
        onTheWay: Number,
        stockAlert: Number
    },
    warehouseID: { type: Schema.Types.ObjectId, ref: "warehouse" },
    rackID: { type: Schema.Types.ObjectId, ref: "rack" }
}, {
    versionKey: false,
})
WarehouseSKU.plugin(timestamps);
module.exports = WarehouseSKU;