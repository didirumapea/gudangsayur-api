const mongoose = require('mongoose');
const { Schema } = mongoose;
const timestamps = require('mongoose-unix-timestamp-plugin');

const Shopauthentication = new Schema({
  marketplace: { type: String },
  userID: { type: Schema.Types.ObjectId, ref: "user" },
  shopName: { type: String },
  shopID: { type: String },
  country: { type: String },
  accessToken: { type: String },
  refreshToken: { type: String },
  expiredIn: { type: Number, default: null }
}, {
  versionKey: false,
})

Shopauthentication.plugin(timestamps);
module.exports = Shopauthentication;