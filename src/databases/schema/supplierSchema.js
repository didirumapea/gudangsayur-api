const mongoose = require('mongoose');
const timestamps = require('mongoose-unix-timestamp-plugin');
const { Schema } = mongoose;

const Supplier = new Schema({
    userID: { type: Schema.Types.ObjectId, ref: "user" },
    name: { type: String, required: true },
    settlement: { type: String, required: true },
    phone: { type: String, default: null },
    email: { type: String, default: null },
    note: { type: String, default: null },
    address: { type: String, default: null },
    website: { type: String, default: null }
}, {
    versionKey: false,
})
Supplier.plugin(timestamps);
module.exports = Supplier;