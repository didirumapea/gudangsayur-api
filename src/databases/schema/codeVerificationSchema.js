const mongoose = require('mongoose');
const timestamps = require('mongoose-unix-timestamp-plugin');
const { Schema } = mongoose;

const Codeverification = new Schema({
    email: { type: String },
    code:  { type: String },
   
}, {
    versionKey: false,
    // toJSON: { virtuals: true },
   
 })
Codeverification.plugin(timestamps);
module.exports = Codeverification;