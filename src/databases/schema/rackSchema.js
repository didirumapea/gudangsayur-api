const mongoose = require('mongoose');
const timestamps = require('mongoose-unix-timestamp-plugin');
const { Schema } = mongoose;

const Rack = new Schema({
    warehouseID: { type: Schema.Types.ObjectId, ref: "warehouse" },
    name: { type: String, required: true },
    note: { type: String, default: null },
    status: { type: Number, default: 1 },
}, {
    versionKey: false,
})
Rack.plugin(timestamps);
module.exports = Rack;