const mongoose = require('mongoose');
const timestamps = require('mongoose-unix-timestamp-plugin');
const { Schema } = mongoose;

const Applist = new Schema({
    marketplace: { type: String },
    appid: { type: Number },
    appsecret: { type: String },
    env: { type: String },
    apicallback: { type: String },
    apiwebhook: { type: String },
    description: { type: String },
}, {
    versionKey: false,
})
Applist.plugin(timestamps);
module.exports = Applist;